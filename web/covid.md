---
layout: page
title: "COVID-19"
permalink: /covid/
---

# COVID-19 Policy
The Linux Application Summit committee recognizes that the COVID-19 pandemic is still ongoing and will take some steps to minimize risk to everyone:
  * There will be hand sanitizer stationed in a few different high-traffic venue locations.
  * At the registration booth, there will be limited surgical masks available on request (free).

LAS follows all local government requirements which means no mandatory COVID-19 measures.

Relevant Czech Republic COVID-19 government policy and a list of testing centers can be found at the following links:
  * [Ministry of the Interior of the Czech Republic - COVID-19 info](https://www.mvcr.cz/mvcren/article/coronavirus-information-of-moi.aspx)
  * [Visiting the Czech Republic - What you should know before you go](https://www.visitczechrepublic.com/en-US/926179fc-1601-4cf8-b201-0b33b3878f08/page/covid-19)
  * [COVID-19 Testing Centers](https://en.mapy.cz/zakladni?q=testing%20center%20coronavirus&cat=1&x=16.6078275&y=49.2023367&z=14)
