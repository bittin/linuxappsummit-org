---
layout: page
title: "Sponsors"
permalink: /sponsors/
style: sponsors-page
---

# LAS Sponsors

Would you like to help make the Linux App Summit possible? Join us and [sponsor](https://linuxappsummit.org/sponsor/)!

## SUPPORTER

### TUXEDO Computers

<img class="sponsorlogo" src="/assets/tuxedo.svg" alt="TUXEDO"/>

> TUXEDO Computers builds tailor-made hardware with Linux!
>
> For a wide variety of computers and notebooks - all of them individually
built und prepared - we are the place to go. From lightweight ultrabooks
up to full-fledged AI development stations TUXEDO covers every aspect of
modern Linux-based computing.
> In addition to that we provide customers with full service at no extra
cost: Self-programmed driver packages, tech support, fully automated
installation services and everything around our hardware offerings.
>
> [https://www.tuxedocomputers.com](https://www.tuxedocomputers.com)

## Community Patron

### Thunderbird

<img class="sponsorlogo" src="/assets/Thunderbird.png" alt="Thunderbird"/>

> Thunderbird is the best open source email client and personal information manager for Linux. The privacy-respecting power tool for managing your Inbox, Calendars, and Tasks.

## Media Partners

### FOSS Life

<img class="sponsorlogo" src="/assets/fosslife.png" alt="FOSS Life"/>

> [FOSSlife](https://www.fosslife.org/) is dedicated to the world of free and open source software, focusing on careers, skills, and resources to help you build your future with FOSS. We provide timely information, useful insight, and practical resources for those who want to build or advance their career with open source.

### Linux Magazine

<img class="sponsorlogo" src="/assets/linux-magazine.png" alt="Linux Magazine"/>

> [Linux Magazine](https://www.linux-magazine.com/) keeps the emphasis on real-life, practical techniques, which has helped make it one of the fastest growing Linux magazines worldwide. They deliver insightful technical articles on a range of topics related to IT technology.

### Open Source Job Hub

<img class="sponsorlogo" src="/assets/open-source-job-hub.png" alt="Open Source Job Hub"/>

> [Open Source JobHub](https://opensourcejobhub.com/) aims to help everyone find a place in the open source ecosystem by connecting job seekers with employers looking for top talent. Let us help you turn down the noise and find the perfect job fit.
