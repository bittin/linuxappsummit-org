---
layout: page
title: "Location + Travel"
permalink: /local/
---

## Monterrey, Mexico

LAS 2024 will take place in Monterrey! This page provides more details about our location and venue, some limited guidance on where to eat and stay, some things you might want to check out if you have extra time, and logistical information.

---

## Table of Contents
**[About Monterrey](#about)**  
**[Venue](#venue)**  
**[Getting To/From Monterrey](#getting-there)**   
**[Where to Sleep/Food/Drink](#capitalism)**   

---

<div id="about"/>

## About Monterrey
Monterrey is the second-largest city in Mexico, just behind Mexico City. It is located in the north-east region of Mexico. 

Find more information about Monterrey on the city's [official website](https://www.monterrey.gob.mx/) (Spanish only).

![aerial view of Monterrey](/assets/monterrey.jpg)  
Image by Mexico en Fotos on Wikimedia


<div id="venue"/>

## The Venue

The conference will take place at:

[Monterrey Institute of Technology and Higher Education (ITESM), Monterrey Campus](https://tec.mx/en)  
Av. Eugenio Garza Sada 2501 Sur, Tecnológico, 64849 Monterrey, N.L., Mexico  
La Carreta is building #4 on the provided campus map.

![img of ITESM map](/assets/tec-of-monterrey-campus-map.jpg)

ITESM was founded in 1943 by Eugenio Garza Sada, an MIT-educated industrialist. It was the first university to be connected to the Internet in Ibero-America. It has the top-ranked business school in the region, according to the *Economist*, and is one of the leaders in patent applications among Mexican universities. The medical school, in partnership with Houston Methodist Hospital, offers the only graduate program available in Mexico.

![img of ITESM - La Carreta building](/assets/la-carreta.jpg)

<div id="getting-there"/>

## Getting to Monterrey

### Visa restrictions
These are general guidelines, please double-check with your local Mexican consulate if there are different requirements for you.

#### Check if you need a VISA
[Countries](https://www.inm.gob.mx/gobmx/word/index.php/paises-no-requieren-visa-para-mexico/) that DON'T need a visa.
[Countries](https://www.inm.gob.mx/gobmx/word/index.php/paises-requieren-visa-para-mexico/) that DO need a visa.

You won't need a visa if you have a valid multiple-entry US visa (you can use it to enter Mexico for up to 180 days without a Mexican Visitor visa, as long as your US visa is valid during your stay and you don't work while in Mexico) or a document proving proof of permanent residence in **Canada**, **USA**, **Japan**, **UK**, or any of the countries that make up the **Schengen Area**.
Everyone needs to provide basic documentation on arrival. You must have a valid passport under international law, e.g., valid for six months beyond your intended return date, and the FMM form provided by the airlines during the flight.
The immigration authority may verify the reason for the trip, using any of the following documents:

1. Hotel reservation and return tickets (itinerary).
2. Invitation letter of the organization or institution inviting the foreigner to participate in any unpaid activity.

#### How to get a visa
Before you travel, you will need to apply for a Mexican visa providing the following documentation:

1. The visa [application form](https://embamex.sre.gob.mx/australia/images/pdf/solicitudvisaingles.pdf) must be legible and the signature must be the same as the passport.
2. A valid passport under international law, e.g., one that is valid for six months beyond your intended return date.
3. A photograph with a white background, with the face visible and without glasses, in color. The photograph dimensions should be 32 millimeters x 26 millimeters or 39 millimeters x 31 millimeters.
4. Original and copy of the document proving your legal stay in the country where you are requesting the visa from.
5. Proof of solvency or rooting.
6. Original and copy of proof of investments or bank accounts balances of the last three months with an average monthly balance equivalent to 1,400 euros, or, original and copy of the documents proving that you have employment or pension with monthly income free of liens equivalent to 500 euros.
7. Original and copy of proof of stable employment with a minimum age of two years, original and copy of public deed of real estate duly registered in the name of the applicant with a minimum age of two years and document proving ownership or participation in business issued by the competent authorities with a minimum age of two years.

#### Asking for an LAS Invitation Letter
Please register and indicate on your form that you need a visa letter. You can also contact us at info@linuxappsummit.org to request a letter.

#### Asking for the Accommodation Document
You should ask your host/hotel/residence to send you a confirmation of your reservation.
Once you gathered all the documentation, print the requirements and go to the [MEXITEL website](https://citas.sre.gob.mx/) to schedule an appointment.
Applying for a Mexican visa costs approximately $53 USD.

### Resources regarding visa
* http://www.sectur.gob.mx/guia-de-viaje/visa/
* https://www.inm.gob.mx/gobmx/word/index.php/paises-no-requieren-visa-para-mexico/
* https://www.inm.gob.mx/gobmx/word/index.php/paises-requieren-visa-para-mexico/
* https://embamex.sre.gob.mx/australia/images/pdf/solicitudvisaingles.pdf (English version)
* https://embamex.sre.gob.mx/italia/images/pdf/consulado/sol-visa-2018.pdf (Spanish version)
* https://citas.sre.gob.mx/

### By Bus  
Monterrey International bus station has daily trips from/to Monterrey to all the states in México and several USA states including Texas, California, among others. Tickets range from $5 to $150 depending on the destination.

### By Plane  
Monterrey International airport “General Mariano Escobedo” (IATA code: MTY) is well connected and has daily domestic and international flights, including several flights a day to hubs in the USA (HOU, DFW, LAX, MIA, ATL, ORD) and a direct flight to Madrid Barajas (MAD).

## Getting around Monterrey
### Local public transportation
Monterrey is well connected by public transportation, including bus and taxi.  
* Bus: Typically the bus stops are within 200 meters apart covering the entire city. The current price is $0.80 USD per ticket. Buses run approximately 15 minutes apart.  
* Taxis: The cost may vary from $2 to $30 USD for a full end-to-end city trip.
* Other ride services: Monterrey also has the major app-based players including Uber, Didi, and Cabify.

<div id="capitalism"/>

## Where to Stay

There are several accommodation options in the city. The high availability and price options will make sure that everyone can find an accommodation option that will satisfy their personal taste and budget. Monterrey has some worldwide chain hotels including IHG, Hilton, Marriot, and many more. The cheapest options will start from approximately $10 USD for a dormitory bed in a hostel. For a stay in a 4* hotel, the price of a double room is typically around $25-$35 USD per night. 5* hotels can cost up to $150 USD per night for a double room. Other options are app-based apartments such as AirBnB, with prices ranging from $10 USD per night up to $100 USD for a full house/night.


#### Fiesta Inn Monterrey Tecnológico
- ~ $95 USD standard room - does not include breakfast
- 4-stars
- 14 minutes to ITESM (1km)
- https://www.fiestainn.com/hoteles/fiesta-inn-monterrey-tecnologico


#### One Monterrey Tecnológico
- ~ $70 USD standard room - includes breakfast
- 3-stars
- 18 minutes to ITESM (1.2km)
- https://www.onehoteles.com/hoteles/one-monterrey-tecnologico


#### Holiday Inn Express
- ~ $95 USD standard room - includes breakfast
- 3-stars
- 24 minutes to ITESM (1.8km)
- https://www.ihg.com/holidayinnexpress/hotels/us/en/monterrey/mtyeg/hoteldetail
